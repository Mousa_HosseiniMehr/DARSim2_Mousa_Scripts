% %% RUN ALL
% Input1 = 'C:\Users\lucp9354\Documents\DARSim2\SPE10T\FineScale';
% Input1 = 'C:\Users\lucp9354\Documents\DARSim2\SPE10T\ADM_0.1'
% Input2 = 'C:\Users\lucp9354\Documents\DARSim2\SPE10T\ADM_0.5'
% Input3 = 'C:\Users\lucp9354\Documents\DARSim2\SPE10T\Homogenization_0.1';
% Input4 = 'C:\Users\lucp9354\Documents\DARSim2\SPE10T\Homogenization_0.5';
% Dirperm = 'C:\Users\lucp9354\Documents\DARSim2\SPE10T'
% % 
% DARSim2ResSim(Input1,'Run.txt',Dirperm);
% DARSim2ResSim(Input2,'Run.txt',Dirperm);
% DARSim2ResSim(Input3,'Run.txt',Dirperm);
% DARSim2ResSim(Input4,'Run.txt',Dirperm);

%%
close all; clear all; clc;
Directory = 'F:\Simulation\DARSim2\Manuela_BF=Const\SPE10T';
% 
% tol = [0.1 0.3 0.5];
% N_Comparison = 3;
% 
% Nx = 216;
% Ny = 054;
% Nz = 001;
% Nf = 0;
% N_tstep = 100;
% 
% Error_P_ADM  = zeros(N_tstep,N_Comparison);
% Error_P_Homo = zeros(N_tstep,N_Comparison);
% Error_S_ADM  = zeros(N_tstep,N_Comparison);
% Error_S_Homo = zeros(N_tstep,N_Comparison);
% ActiveCells_ADM  = cell(N_Comparison,1);
% ActiveCells_Homo = cell(N_Comparison,1);

%% Looping over time to compute pressure and saturation errors
% for t = 1:N_tstep
%     
%     fprintf('Checking timestep: %3.0f\n', t);
% 	
%     % Reading finescale files
% 	File = strcat(Directory, '\FineScale\Output\Solution\ExampleT_Sol',num2str(t),'.txt');
% 	FileID = fopen(File, 'r');
% 	Matrix = textscan(FileID, '%s');
% 	Matrix = str2double(Matrix{1,1});
% 	Matrix = reshape(Matrix,[3,Nx*Ny*Nz+Nf])';
% 	fclose(FileID);
% 	P_FineScale = Matrix(:,2);
% 	S_FineScale = Matrix(:,3);
% 	
% 	% Reading ADM files
%     for n = 1:N_Comparison
%         File = strcat(Directory, '\ADM_',num2str(tol(n)),'\Output\Solution\ExampleT_Sol',num2str(t),'.txt');
% 		FileID = fopen(File, 'r');
% 		Matrix = textscan(FileID, '%s');
% 		Matrix = str2double(Matrix{1,1});
% 		Matrix = reshape(Matrix,[3,Nx*Ny*Nz+Nf])';
% 		fclose(FileID);
% 		P_ADM{n} = Matrix(:,2);
% 		S_ADM{n} = Matrix(:,3);
%         Error_P_ADM(t,n) = norm( (P_FineScale - P_ADM{n}) ) / norm( P_FineScale );
%         Error_S_ADM(t,n) = norm( (S_FineScale - S_ADM{n}) ) / norm( S_FineScale );
%         
%         
%         File = strcat(Directory, '\Homogenization_',num2str(tol(n)),'\Output\Solution\ExampleT_Sol',num2str(t),'.txt');
%         FileID = fopen(File, 'r');
% 		Matrix = textscan(FileID, '%s');
% 		Matrix = str2double(Matrix{1,1});
% 		Matrix = reshape(Matrix,[3,Nx*Ny*Nz+Nf])';
% 		fclose(FileID);
% 		P_Homo{n} = Matrix(:,2);
% 		S_Homo{n} = Matrix(:,3);
%         Error_P_Homo(t,n) = norm( (P_FineScale - P_Homo{n}) ) / norm( P_FineScale );
%         Error_S_Homo(t,n) = norm( (S_FineScale - S_Homo{n}) ) / norm( S_FineScale );
%         
%     end
% end

%% Reading ADMStats files
% for n = 1:N_Comparison
%     File = strcat(Directory, '\ADM_',num2str(tol(n)),'\Output\ADMStats.txt');
%     FileID = fopen(File, 'r');
%     Matrix = textscan(FileID, '%s');
%     Matrix = str2double(Matrix{1,1});
%     Matrix = reshape(Matrix,[4,length(Matrix)/4])';
%     fclose(FileID);
%     ActiveCells_ADM{n} = Matrix(:,4);
%     
%     File = strcat(Directory, '\Homogenization_',num2str(tol(n)),'\Output\ADMStats.txt');
%     FileID = fopen(File, 'r');
%     Matrix = textscan(FileID, '%s');
%     Matrix = str2double(Matrix{1,1});
%     Matrix = reshape(Matrix,[4,length(Matrix)/4])';
%     fclose(FileID);
%     ActiveCells_Homo{n} = Matrix(:,4);
% end
% 
% %% Computing average values
% Error_P_ADM_Ave  = zeros(1,length(tol));
% Error_S_ADM_Ave  = zeros(1,length(tol));
% Error_P_Homo_Ave = zeros(1,length(tol));
% Error_S_Homo_Ave = zeros(1,length(tol));
% ActiveCells_ADM_Ave = zeros(1,length(tol));
% ActiveCells_Homo_Ave = zeros(1,length(tol));
% for n = 1 : N_Comparison
%     Error_P_ADM_Ave(n)  = mean(Error_P_ADM(:,n));
%     Error_S_ADM_Ave(n)  = mean(Error_S_ADM(:,n));
%     Error_P_Homo_Ave(n) = mean(Error_P_Homo(:,n));
%     Error_S_Homo_Ave(n) = mean(Error_S_Homo(:,n));
%     ActiveCells_ADM_Ave(n) = mean(ActiveCells_ADM{n});
%     ActiveCells_Homo_Ave(n) = mean(ActiveCells_Homo{n});
% end

%% Saving the Workspace
% save(strcat(Directory,'\matlab.mat'));

%% Loading the Workspace
Directory = 'F:\Simulation\DARSim2\Manuela_BF=Const\SPE10T';
load(strcat(Directory,'\matlab.mat'));
Directory = 'F:\Simulation\DARSim2\Manuela_BF=Const\SPE10T';

%% Plotting attributes
figLineType = { '-' , '--' , '-.' , '-*' };
MarkerIndex = round(linspace(1,N_tstep,15));
legendText = cell(N_Comparison,1);
m=0;
for n = 1:N_Comparison
    m = m+1;
    legendText(m) = strcat( {'ADM-MMs: tol '} , char(916) , 'S=' , num2str(tol(n)) );
    m = m+1;
    legendText(m) = strcat( {'ADM-Hom: tol '} , char(916) , 'S=' , num2str(tol(n)) );
end

%% Plotting pressure error
fig = figure;
hold on;
for n = 1:N_Comparison
    plot( (1:N_tstep) , Error_P_ADM(:,n)  , strcat(figLineType{n},'o') , 'LineWidth',2 ,'Color','r' , 'MarkerIndices',MarkerIndex , 'MarkerSize',10 );
    plot( (1:N_tstep) , Error_P_Homo(:,n) , strcat(figLineType{n},'x') , 'LineWidth',2 ,'Color','b' , 'MarkerIndices',MarkerIndex , 'MarkerSize',10 );
end
xlim([1,N_tstep]);
set(gca,'fontsize',15);
set(gcf, 'Position', [100, 100, 800, 500]);
grid on;
title('Pressure Error');
legend(legendText,'FontSize', 15);
xlabel('Time-step','FontSize',15);
ylabel('Error(P)','FontSize',15);
saveas(fig, strcat(Directory, '\Pressure_Error.fig'));
saveas(fig, strcat(Directory, '\Pressure_Error.png'));

%% Plotting saturation error
fig = figure;
hold on;
for n = 1:N_Comparison
    plot( (1:N_tstep) , Error_S_ADM(:,n)  , strcat(figLineType{n},'s') , 'LineWidth',2 ,'Color','r' , 'MarkerIndices',MarkerIndex , 'MarkerSize',10 );
    plot( (1:N_tstep) , Error_S_Homo(:,n) , strcat(figLineType{n},'*') , 'LineWidth',2 ,'Color','b' , 'MarkerIndices',MarkerIndex , 'MarkerSize',10 );
end
xlim([1,N_tstep]);
set(gca,'fontsize',15);
set(gcf, 'Position', [100, 100, 800, 500]);
grid on;
title('Saturation Error');
legend(legendText,'FontSize', 15);
xlabel('Time-step','FontSize',15);
ylabel('Error(S)','FontSize',15);
saveas(fig, strcat(Directory, '\Saturation_Error.fig'));
saveas(fig, strcat(Directory, '\Saturation_Error.png'));

%% Plotting active grid cells
fig = figure;
hold on;
for n = 1:N_Comparison
    plot( (1:N_tstep) , ActiveCells_ADM{n}(1:N_tstep)  , strcat(figLineType{n},'d') , 'LineWidth',2 ,'Color','r' , 'MarkerIndices',MarkerIndex , 'MarkerSize',10 );
    plot( (1:N_tstep) , ActiveCells_Homo{n}(1:N_tstep) , strcat(figLineType{n},'+') , 'LineWidth',2 ,'Color','b' , 'MarkerIndices',MarkerIndex , 'MarkerSize',10 );
end
xlim([1,N_tstep]);
set(gca,'fontsize',15);
set(gcf, 'Position', [100, 100, 800, 500]);
grid on;
title('Percentage of Active Grid Cells over Time-steps');
legend(legendText,'FontSize', 15);
xlabel('Time-step','FontSize',15);
ylabel('% Active Grid Cells','FontSize',15);
saveas(fig, strcat(Directory, '\Active_Grids.fig'));
saveas(fig, strcat(Directory, '\Active_Grids.png'));

%% Plotting average values for pressure error, saturation error and active grid cells for different tolerances
fig = figure;
hold on;
yyaxis left;  plot( tol , Error_P_ADM_Ave          , '-o'  , 'LineWidth',2 ,'Color','r' , 'MarkerSize',12 );
yyaxis left;  plot( tol , Error_P_Homo_Ave         , '-x'  , 'LineWidth',2 ,'Color','b' , 'MarkerSize',12 );
yyaxis right; plot( tol , Error_S_ADM_Ave          , '--s' , 'LineWidth',2 ,'Color','r' , 'MarkerSize',12 );
yyaxis right; plot( tol , Error_S_Homo_Ave         , '--*' , 'LineWidth',2 ,'Color','b' , 'MarkerSize',12 );
yyaxis right; plot( tol , ActiveCells_ADM_Ave/100  , '-.d' , 'LineWidth',2 ,'Color','r' , 'MarkerSize',12 );
yyaxis right; plot( tol , ActiveCells_Homo_Ave/100 , '-.+' , 'LineWidth',2 ,'Color','b' , 'MarkerSize',12 );
set(gca,'fontsize',15);
set(gcf, 'Position', [100, 100, 800, 500]);
grid on;
legend({ 'Average Perssure Error ADM-MMs (left axis)'     , 'Average Perssure Error ADM-Hom (left axis)'    , ...
         'Average Saturation Error ADM-MMs (right axis)'  , 'Average Saturation Error ADM-Hom (right axis)' , ...
         'Avergare Active Cells ADM-MMs[-] (right axis)' , 'Avergare Active Cells ADM-Hom [-] (right axis)' } ,'FontSize', 15, 'Location', 'north');
xlabel('ADM Tolerance','FontSize',15);
ylabel('[-]','FontSize',15);
saveas(fig, strcat(Directory, '\Average_Values.fig'));
saveas(fig, strcat(Directory, '\Average_Values.png'));