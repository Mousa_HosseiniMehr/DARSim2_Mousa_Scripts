%%%%%%%%%%%%%%%%%%%%%%%%%% DNS_vs_pEDFM_vs_EDFM %%%%%%%%%%%%%%%%%%%%%%%%%%%
% Author: Mousa HosseiniMehr
% Date: 2019-07-16
%
% This script compares the results of EDFM and pEDFM with DNS and
% calculates the error for each.

%%
close all; clear all; clc;
Directory = 'F:\Simulation\DARSim2\InputDesktopWork\pEDFM_FineScale\LowPerm';
Nx = [375;75;45;25];
Ny = [375;75;45;25];
Nz = [001;01;01;01];
Nf = [000,75,45,25];
Lx = 3.0; dx = Lx./Nx;
Ly = 3.0; dy = Ly./Ny;
Cfx = Nx(1)./Nx(2:end);
Cfy = Ny(1)./Ny(2:end);
Cfz = Nz(1)./Nz(2:end); Cfz(Nz(2:end)==1)=0;
Nt = 50;
Total_Time = 12; % [hours]
dt = Total_Time/Nt; %[day]
for n = 1:length(Nx)
    xcm{n} = linspace(dx(n)/2 , Lx-dx(n)/2 , Nx(n))';
    ycm{n} = linspace(dy(n)/2 , Ly-dy(n)/2 , Ny(n))';
end

%%
Error_P_pEDFM = zeros(Nt,length(Nx)-1);
Error_T_pEDFM = zeros(Nt,length(Nx)-1);
Error_P_EDFM  = zeros(Nt,length(Nx)-1);
Error_T_EDFM  = zeros(Nt,length(Nx)-1);

%% Reading the simulation files
for t = 1:Nt
    fprintf('Checking timestep: %d\n', t);
    
    % Reading DNS Files
    File = strcat(Directory, '\DNS_375x375\Output\Solution\SinglePhaseGeo_Sol',num2str(t),'.txt');
    FileID = fopen(File, 'r');
    Matrix = textscan(FileID, '%s');
    Matrix = str2double(Matrix{1,1});
    Matrix = reshape(Matrix,[5,Nx(1)*Ny(1)*Nz(1)])';
    fclose(FileID);
    P_DNS = Matrix(:,2);
    T_DNS = Matrix(:,4);
    
    % Reading EDFM and pEDFM Files
    P_pEDFM = cell(length(Nx)-1,1);
    T_pEDFM = cell(length(Nx)-1,1);
    P_EDFM  = cell(length(Nx)-1,1);
    T_EDFM  = cell(length(Nx)-1,1);
    
    for n = 2:length(Nx)
        % Reading pEDFM Files
        File = strcat(Directory, '\pEDFM_',num2str(Nx(n)),'x',num2str(Nx(n)),'\Output\Solution\SinglePhaseGeo_Sol',num2str(t),'.txt');
        FileID = fopen(File, 'r');
        Matrix = textscan(FileID, '%s');
        Matrix = str2double(Matrix{1,1});
        Matrix = reshape(Matrix,[5,Nx(n)*Ny(n)*Nz(n)+Nf(n)])';
        fclose(FileID);
        P_pEDFM{n-1} = Matrix(:,2);  P_pEDFM{n-1}(Nx(n)*Ny(n)*Nz(n)+1:end)=[];
        T_pEDFM{n-1} = Matrix(:,4);  T_pEDFM{n-1}(Nx(n)*Ny(n)*Nz(n)+1:end)=[];
        
        % Reading EDFM Files
        File = strcat(Directory, '\EDFM_',num2str(Nx(n)),'x',num2str(Nx(n)),'\Output\Solution\SinglePhaseGeo_Sol',num2str(t),'.txt');
        FileID = fopen(File, 'r');
        Matrix = textscan(FileID, '%s');
        Matrix = str2double(Matrix{1,1});
        Matrix = reshape(Matrix,[5,Nx(n)*Ny(n)*Nz(n)+Nf(n)])';
        fclose(FileID);
        P_EDFM{n-1} = Matrix(:,2);  P_EDFM{n-1}(Nx(n)*Ny(n)*Nz(n)+1:end)=[];
        T_EDFM{n-1} = Matrix(:,4);  T_EDFM{n-1}(Nx(n)*Ny(n)*Nz(n)+1:end)=[];
        
    end

    % Comparing with reference solution
    P_ref = cell(length(Nx)-1,1);
    T_ref = cell(length(Nx)-1,1);
    for n = 2:length(Nx)
        P_ref{n-1} = zeros(Nx(n)*Ny(n)*Nz(n),1);
        T_ref{n-1} = zeros(Nx(n)*Ny(n)*Nz(n),1);
        for k = 1:Nz(n)
            for j = 1:Ny(n)
                for i =1:Nx(n)
                    I_EDFM = Nx(n)*Ny(n)*(k-1) + Nx(n)*(j-1) + i;
                    I_ref  = Nx(1)*Ny(1)*(Cfz(1)*(k-1)+round(Cfz(1)/2)) + Nx(1)*(Cfy(1)*(j-1)+round(Cfy(1)/2)) + Cfx(1)*(i-1)+round(Cfx(1)/2);
                    P_ref{n-1}(I_EDFM) = P_DNS(I_ref);
                    T_ref{n-1}(I_EDFM) = T_DNS(I_ref);
                end
            end
        end
        Error_P_pEDFM(t,n-1) = norm( (P_ref{n-1} - P_pEDFM{n-1}) ) / norm( P_ref{n-1} );
        Error_T_pEDFM(t,n-1) = norm( (T_ref{n-1} - T_pEDFM{n-1}) ) / norm( T_ref{n-1} );
        Error_P_EDFM(t,n-1)  = norm( (P_ref{n-1} - P_EDFM{n-1} ) ) / norm( P_ref{n-1} );
        Error_T_EDFM(t,n-1)  = norm( (T_ref{n-1} - T_EDFM{n-1} ) ) / norm( T_ref{n-1} );
    end
end


%% Manual Plot for 3 comparisons
fig = figure;
hold on;
plot( (1:t)*dt , Error_P_pEDFM(:,1) , '-o'  , 'LineWidth',2 , 'Color','r' );
plot( (1:t)*dt , Error_P_pEDFM(:,2) , '--s' , 'LineWidth',2 , 'Color','r' );
plot( (1:t)*dt , Error_P_pEDFM(:,3) , '-.d' , 'LineWidth',2 , 'Color','r' );
plot( (1:t)*dt , Error_P_EDFM(:,1)  , '-x'  , 'LineWidth',2 , 'Color','b' );
plot( (1:t)*dt , Error_P_EDFM(:,2)  , '--*' , 'LineWidth',2 , 'Color','b' );
plot( (1:t)*dt , Error_P_EDFM(:,3)  , '-.+' , 'LineWidth',2 , 'Color','b' );
title('Pressure Error');
legend('DNS(375\times375) vs pEDFM(75\times75)' , 'DNS(375\times375) vs pEDFM(45\times45)' , 'DNS(375\times375) vs pEDFM(25\times25)' , ...
       'DNS(375\times375) vs EDFM(75\times75)'  , 'DNS(375\times375) vs EDFM(45\times45)'  , 'DNS(375\times375) vs EDFM(25\times25)'        );
xlabel('Simulation Time [hours]');
ylabel('Pressure Error');
xlim([1,Nt]*dt);
set(gca,'fontsize',15);
set(gcf, 'Position', [100, 100, 800, 500]);
grid on;
saveas(fig, strcat(Directory, '\Error_P.fig'));
saveas(fig, strcat(Directory, '\Error_P.png'));

%% Manual Plot for 3 comparisons
fig = figure;
hold on;
plot( (1:t)*dt , Error_T_pEDFM(:,1) , '-o'  , 'LineWidth',2 , 'Color','r' );
plot( (1:t)*dt , Error_T_pEDFM(:,2) , '--s' , 'LineWidth',2 , 'Color','r' );
plot( (1:t)*dt , Error_T_pEDFM(:,3) , '-.d' , 'LineWidth',2 , 'Color','r' );
plot( (1:t)*dt , Error_T_EDFM(:,1)  , '-x'  , 'LineWidth',2 , 'Color','b' );
plot( (1:t)*dt , Error_T_EDFM(:,2)  , '--*' , 'LineWidth',2 , 'Color','b' );
plot( (1:t)*dt , Error_T_EDFM(:,3)  , '-.+' , 'LineWidth',2 , 'Color','b' );
title('Temperature Error');
legend('DNS(375\times375) vs pEDFM(75\times75)' , 'DNS(375\times375) vs pEDFM(45\times45)' , 'DNS(375\times375) vs pEDFM(25\times25)' , ...
       'DNS(375\times375) vs EDFM(75\times75)'  , 'DNS(375\times375) vs EDFM(45\times45)'  , 'DNS(375\times375) vs EDFM(25\times25)'        );
xlabel('Simulation Time [hours]');
ylabel('Temperature Error');
xlim([1,Nt]*dt);
set(gca,'fontsize',15);
set(gcf, 'Position', [100, 100, 800, 500]);
grid on;
saveas(fig, strcat(Directory, '\Error_T.fig'));
saveas(fig, strcat(Directory, '\Error_T.png'));