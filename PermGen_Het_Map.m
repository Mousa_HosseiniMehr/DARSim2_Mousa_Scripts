close all; clear all; clc;
Directory = 'F:\Simulation\DARSim2\Permeability\Manuela\make\';
Nx = 220;
Ny = 60;
Nz = 01;

File = strcat(Directory, 'kk_t');
FileID = fopen(File, 'r');
Matrix = textscan(FileID, '%s');
Matrix = str2double(Matrix{1,1});
Matrix = Matrix(1:2:end);
Matrix = reshape(Matrix,Ny,Nx);
Matrix = Matrix';
fclose(FileID);

K = zeros(Nx*Ny*Nz,1);
for k = 1:Nz
    for j = 1:Ny
        Index = Nx*Ny*(k-1) + Nx*(j-1) + (1:Nx)';
        K(Index) = Matrix(1:Nx,j,k);
    end
end

Z = reshape(K,Nx,Ny)';
surf(log10(Z));

File = strcat(Directory, 'Perm_SPE10T.txt');
delete(File);
fid = fopen(File,'a+');
fprintf(fid, '%1.6e\n',Nx);
fprintf(fid, '%1.6e\n',Ny);
fprintf(fid, '%1.6e\n',Nz);
fprintf(fid, '%1.6e\n',K);
fclose('all');