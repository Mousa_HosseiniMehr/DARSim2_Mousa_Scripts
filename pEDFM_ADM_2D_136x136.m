%% SinglePhaseGeo pEDFM-ADM 2D 136x136 Homogeneous with 30 LowConductiveFractures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_2D_136x136_30frac_pEDFM\MixedConductiveFractures_2\Homogeneous\ADM_05';
% Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
% save(strcat(Directory,'\Output\Result.mat'),'Result');
% clear all;
% 
% Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_2D_136x136_30frac_pEDFM\MixedConductiveFractures_2\Homogeneous\ADM_10';
% Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
% save(strcat(Directory,'\Output\Result.mat'),'Result');
% clear all;
% 
% Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_2D_136x136_30frac_pEDFM\MixedConductiveFractures_2\Homogeneous\ADM_20';
% Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
% save(strcat(Directory,'\Output\Result.mat'),'Result');
% clear all;
% 
% Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_2D_136x136_30frac_pEDFM\MixedConductiveFractures_2\Homogeneous\ADM_50';
% Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
% save(strcat(Directory,'\Output\Result.mat'),'Result');
% clear all;

Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_2D_136x136_30frac_pEDFM\MixedConductiveFractures_2\Homogeneous\FineScale';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;


%% SinglePhaseGeo pEDFM-ADM 2D 136x136 Heterogeneous with 30 LowConductiveFractures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_2D_136x136_30frac_pEDFM\MixedConductiveFractures_2\Heterogeneous\ADM_05';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;

Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_2D_136x136_30frac_pEDFM\MixedConductiveFractures_2\Heterogeneous\ADM_10';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;

Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_2D_136x136_30frac_pEDFM\MixedConductiveFractures_2\Heterogeneous\ADM_20';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;

Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_2D_136x136_30frac_pEDFM\MixedConductiveFractures_2\Heterogeneous\ADM_50';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;

Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_2D_136x136_30frac_pEDFM\MixedConductiveFractures_2\Heterogeneous\FineScale';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;