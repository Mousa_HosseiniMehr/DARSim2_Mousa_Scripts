%% SinglePhaseGeo pEDFM-ADM 3D 81x81x27 Homogeneous with 20 LowConductiveFractures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_3D_81x81x27_20frac_pEDFM\MixedConductiveFractures\ADM_05';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;

Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_3D_81x81x27_20frac_pEDFM\MixedConductiveFractures\ADM_10';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;

Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_3D_81x81x27_20frac_pEDFM\MixedConductiveFractures\ADM_20';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;

Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_3D_81x81x27_20frac_pEDFM\MixedConductiveFractures\ADM_50';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;

Directory = 'D:\OneDrive\Simulation\DARSim2\InputDesktopWork\pEDFM_ADM\SinglePhaseGeo_3D_81x81x27_20frac_pEDFM\MixedConductiveFractures\FineScale';
Result = DARSim2ResSim(Directory,'SinglePhaseGeo.txt','D:\OneDrive\Simulation\DARSim2\Permeability');
save(strcat(Directory,'\Output\Result.mat'),'Result');
clear all;